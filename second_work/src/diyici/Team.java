package diyici;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Team {
 public static void main(String[] args) throws Exception {

  // 读取配置文件
  Properties prop = new Properties();
  prop.load(new FileInputStream("resources//config.properties"));
  Enumeration fileName = prop.propertyNames();
  String url =prop.getProperty("url");
 String cookie=prop.getProperty("cookie");
 
// 测试输出url 和  cookie
//  System.out.println(url);
//  System.out.println(cookie);
 
//	定义一张存所有URL的哈希表【
  Set<String> urljh=new HashSet<>();
  Set<Student> Student_Set=new HashSet<>();
//  】
  
  Document document = Jsoup.connect(url).header("Cookie", cookie).get();
  Elements mk = document.getElementsByClass("interaction-row");

 //遍历所有小作业模块的url存到urljh哈希表
  for(int i=0;i<mk.size();i++) {
	if(mk.get(i).toString().contains("课堂完成部分")) {
		String xmk=mk.get(i).attr("data-url");
		urljh.add(xmk);
	}
  }
  
//测试输出哈希表里的url
//for(String s: urljh ){
//	  System.out.println(s);
//  }
  
  Get_Student_snameandsno(urljh,cookie,Student_Set);
  Get_Student_Score(urljh,cookie,Student_Set);
  Student_Sort(Student_Set);

 

//访问到小作业页面    documents为小作业页面html文档
//每项作业里面的小模块
// 测试输出抓取的内容 【
//  String xurl ="https://www.mosoteach.cn/web/index.php?c=interaction_homework&m=homework_result_list&clazz_course_id=CD7AE281-4AF8-11EA-9C7F-98039B1848C6&id=BE971A3C-087E-CFF3-68D0-3E35A659813B";
//  Document documents = Jsoup.connect(xurl).header("Cookie", cookie).get();
//  String xmk =  documents.getElementsByClass("homework-item").get(0).getElementsByClass("appraised-type").get(1).select("span").get(1).text();
//  String a = xmk;
//  System.out.println(a.equals("尚无评分"));
//  】
 }
 private static void Get_Student_snameandsno(Set<String> urljh,String cookie,Set<Student> Student_Set) throws IOException {
	 for(String s:urljh) {
		 String xurl=s;
		 Document documents = Jsoup.connect(xurl).header("Cookie", cookie).get();
		 Elements es = documents.getElementsByClass("homework-item");
		 for(int i=0;i<es.size();i++) {
		 String sname = es.get(i).select("span").get(0).text();
		 String sno = es.get(i).getElementsByClass("member-message").get(0).child(1).text();
//		 System.out.println(sname);
		 Student stu=new Student(sname,sno);
		 Student_Set.add(stu);
		 }
		 break;
	 }
 }
 
private static void Get_Student_Score(Set<String> urljh,String cookie,Set<Student> Student_Set) throws IOException{
	 for(String s:urljh) {
		 String xurl=s;
		 Document documents = Jsoup.connect(xurl).header("Cookie", cookie).get();
		 Elements es = documents.getElementsByClass("homework-item");
		 for(int i=0;i<es.size();i++) {
			 boolean pd = es.get(i).getElementsByClass("homework-info").text().contains("未提交");

			 if(!pd) {
			  String sname = es.get(i).select("span").get(0).text();
			  String xmk = es.get(i).getElementsByClass("appraised-type").get(1).select("span").get(1).text();
			  if(xmk.equals("尚无评分")) {
			  }
			  else {
				  String score = xmk.substring(0,xmk.indexOf("分"));
				  float   f   =   Float.parseFloat(score);  
//				  System.out.println(f);
				  for(Student student:Student_Set) {
					  if(sname.equals(student.getName())) {
//						  System.out.println("找到了");
						  student.addScore(f);
						  break;
					  }
				  }
			  }
			 }
		 }
	 }
	
}
// 把哈希表导入对象数组进行排序等运算
private static void  Student_Sort(Set<Student> Student_Set)  throws Exception{
	 Student[] Sort_student = new Student[100];
	 int i=0;
	 float max=0;
	 int sign=0;
	 float sum=0;
	 Student dtp = new Student();
	 Student maxsno = new Student();
	 Student bjsno = new Student();
	 for(Student student:Student_Set) {
		 Sort_student[i]=new Student();
		 Sort_student[i++]=student;
	  }
	 //按照经验排序
	 for(int j=0;j<i;j++) {
		 for(int k=i-1;k>j;k--) {
			if(Sort_student[k].getScore()>=max)
			{	
				max=Sort_student[k].getScore();
				sign=k;
			}
		 }
		 dtp=Sort_student[j];
		 Sort_student[j]=Sort_student[sign];
		 Sort_student[sign]=dtp;
		 max=dtp.getScore();
	 }

	 //输出
        for (int g = 0; g < i; g++){
            sum += Sort_student[g].getScore();
        }
        System.out.println("最高经验值为：" + Sort_student[0].getScore() + "," + " 最低经验值为：" + Sort_student[i - 1].getScore() +
                "," + " 平均经验值为：" + String.format("%.1f", (sum / i - 1)));

        //创建new File  写入文件
        File writeName = new File("./output.txt");
        writeName.createNewFile();

        //在output.txt文件中重新写入最高经验值，最低经验值，平均经验值
        BufferedWriter out = new BufferedWriter(new FileWriter(writeName));
        out.write("最高经验值为：" + Sort_student[0].getScore() + "," + " 最低经验值为：" + Sort_student[i - 1].getScore() +
                "," + " 平均经验值为：" + String.format("%.1f", (sum / i - 1)));
        out.flush();
        out.close();

        //在output.txt中续写写入学生信息
        BufferedWriter outStudent = new BufferedWriter(new FileWriter(writeName,true));
        for (int g = 0; g < i; g++){
            if (Sort_student[g].getName().length() == 2) {
                System.out.println("姓名：" + Sort_student[g].getName() + "     " + "学号："
                        + Sort_student[g].getSno() + "  " + "经验：" + Sort_student[g].getScore() + "  ");
                outStudent.write("姓名：" + Sort_student[g].getName() + "     " + "学号："
                        + Sort_student[g].getSno() + "  " + "经验：" + Sort_student[g].getScore() + "  " + "\r");
            } else {
                System.out.println("姓名：" + Sort_student[g].getName()
                        + "   " + "学号：" + Sort_student[g].getSno() + "  " + "经验：" + Sort_student[g].getScore() + "  ");
                outStudent.write("姓名：" + Sort_student[g].getName()
                        + "   " + "学号：" + Sort_student[g].getSno() + "  " + "经验：" + Sort_student[g].getScore() + "  " + "\r");
            }
        }
        outStudent.flush();
        outStudent.close();
        //关闭文件
 }
}


